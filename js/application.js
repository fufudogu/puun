(function($) {

    $.fn.navigatable = function(options) {

        var defaults = {
            'prevClass': 'prev-button',
            'nextClass': 'next-button',
            'columnClass': 'month-col',
            'toggleClass': 'toggle-icon',
            'expandClass': 'icon-plus',
            'collapseClass': 'icon-minus',
            'collapsed': true,
            'numCells': 4
        };

        var opts = $.extend(defaults, options);

        return this.each(function() {
            var table = $(this);
            var nextButton = $('.' +opts.nextClass, table);
            var prevButton = $('.' +opts.prevClass, table);
            var cellWidth = $('.' + opts.columnClass, table).outerWidth();
            var cells = $('.month-wrap-col .tbody, .month-wrap-col .thead', table);
            var nameRows = $('.name-col .tbody .row', table);
            var detailRows = $('.detail-col .tbody .row', table);
            var monthRows = $('.month-wrap-col .tbody .row', table);
            var currentIndex = 0;
            var maxCells = $('.row', cells).eq(0).find('.' + opts.columnClass).size() - opts.numCells;

            table.css('overflow', 'hidden');

            var updateCellPosition = function() {
                cells.transit({
                    'marginLeft': -currentIndex * cellWidth
                });
            };

            var next = function() {
                currentIndex++;
                if (currentIndex > maxCells) {
                    currentIndex = 0;
                }
                updateCellPosition();
            };

            var prev = function() {
                currentIndex--;
                if (currentIndex < 0) {
                    currentIndex = maxCells;
                }
                updateCellPosition();
            }

            nextButton.click(function(e) {
                e.preventDefault();
                next();
            });

            prevButton.click(function(e) {
                e.preventDefault();
                prev();
            });

            /**
             * Expand & Collapse.
             */
            var toggle = function(icon) {
                var icon = $(icon);
                var targetId = icon.data('target');
                var target = $('#' + targetId);
                var state = null;
                var idx = nameRows.index(target);
                var monthRow = monthRows.eq(idx);
                var detailRow = detailRows.eq(idx);

                var isVisible = target.is(':visible');

                if (isVisible) {
                    target.closest('.row').stop().animate({'height': 0}, {'queue': false, 'complete': function() {
                        $(this).hide();
                    }})
                    monthRow.stop().animate({'height': 0}, {'queue': false, 'complete': function() {
                        $(this).hide();
                    }})
                    detailRow.stop().animate({'height': 0}, {'queue': false, 'complete': function() {
                        $(this).hide();
                    }})
                    target.data('prev', target.is(':visible'));
                    icon.removeClass(opts.collapseClass);
                    icon.addClass(opts.expandClass);
                    state = 'hide';
                }
                else {
                    target.closest('.row').stop().show().animate({'height': 45}, {'queue': false});
                    monthRow.stop().show().animate({'height': 45}, {'queue': false});
                    detailRow.stop().show().animate({'height': 45}, {'queue': false});
                    target.data('prev', target.is(':visible'));
                    icon.removeClass(opts.expandClass);
                    icon.addClass(opts.collapseClass);
                    state = 'show';
                }

                var _icon = $('.' + opts.toggleClass, target);
                if (_icon.size() > 0) {
                    var _targetId = _icon.data('target');
                    var _target = $('#' + _targetId);

                    if (state === 'hide') {
                        _target.data('prev', _target.is(':visible'));
                        _target.stop().animate({'height': 0}, {'queue': false, 'complete': function() {
                            $(this).hide();
                        }});
                        monthRows.eq(idx + 1).stop().animate({'height': 0}, {'queue': false, 'complete': function() {
                            $(this).hide();
                        }});
                        detailRows.eq(idx + 1).stop().animate({'height': 0}, {'queue': false, 'complete': function() {
                            $(this).hide();
                        }});
                    }
                    else if (state === 'show') {
                        if (_target.data('prev') === true) {
                            _target.data('prev', _target.is(':visible'));
                            _target.stop().show().animate({'height': 45}, {'queue': false});
                            monthRows.eq(idx + 1).stop().show().animate({'height': 45}, {'queue': false});
                            detailRows.eq(idx + 1).stop().show().animate({'height': 45}, {'queue': false});
                        }
                    }
                }
            };

            var collapse = function(icon) {
                var icon = $(icon);
                var targetId = icon.data('target');
                var target = $('#' + targetId);
                var idx = nameRows.index(target);
                var nameRow = target.closest('.row');
                var monthRow = monthRows.eq(idx);
                var detailRow = detailRows.eq(idx);

                nameRow.css({'overflow': 'hidden', 'height': 0}).hide();
                monthRow.css({'overflow': 'hidden', 'height': 0}).hide();
                detailRow.css({'overflow': 'hidden', 'height': 0}).hide();
                icon.removeClass(opts.collapseClass);
                icon.addClass(opts.expandClass);
                
                var _icon = $('.' + opts.toggleClass, target);
                if (_icon.size() === 0) return;

                var _targetId = _icon.data('target');
                var _target = $('#' + _targetId);
                _target.closest('.row').css({'overflow': 'hidden', 'height': 0}).hide();
                monthRows.eq(idx + 1).css({'overflow': 'hidden', 'height': 0}).hide();
                detailRows.eq(idx + 1).css({'overflow': 'hidden', 'height': 0}).hide();
                _icon.removeClass(opts.collapseClass);
                _icon.addClass(opts.expandClass);
            };

            var icons = $('.' + opts.toggleClass, table);
            icons.click(function(e) {
                toggle(this);

                e.preventDefault();
            });

            if (opts.collapsed) {
                icons.each(function() {
                    collapse(this);
                });
            }
        });

    };

    $.fn.moreLess = function(options) {

        var defaults = {
            moreText: 'See more',
            moreButtonClass: 'btn-down',
            moreButtonText: 'Down',
            lessText: 'See less',
            lessButtonClass: 'btn-up',
            lessButtonText: 'Up'
        };

        var opts = $.extend(defaults, options);

        return this.each(function() {

            $(this).click(function(e) {
                var button = $(this);
                var targetId = '#' + button.data('target');
                var target = $(targetId);

                if (button.hasClass(opts.moreButtonClass)) {
                    button.removeClass(opts.moreButtonClass);
                    button.addClass(opts.lessButtonClass);
                    button.html(opts.lessButtonText);
                    var height = target.data('prevHeight') || 'auto';
                    target.stop().animate({'height': height}, {'queue': false, 'complete': function() {
                        target.css('height', 'auto');
                    }});

                    button.parent().find('span').html(opts.lessText);
                }
                else if (button.hasClass(opts.lessButtonClass)) {
                    button.removeClass(opts.lessButtonClass);
                    button.addClass(opts.moreButtonClass);
                    button.html(opts.moreButtonText);
                    target.data('prevHeight', target.height());
                    target.stop().animate({'height': 0}, {'queue': false});

                    button.parent().find('span').html(opts.moreText);
                }

                e.preventDefault();
            });

        });

    };

    $.fn.dropDown = function(options) {

        var defaults = {
            'selectedClass': 'selected',
            'selectorClass': 'month-items',
            'itemClass': 'month-item'
        };

        var opts = $.extend(defaults, options);

        return this.each(function() {
            var selected = $('.' + opts.selectedClass, this);
            var selector = $('.' + opts.selectorClass, this)
            var items = $('.' + opts.itemClass, this);

            selector.addClass('hide');
            selector.addClass('item-selector');

            selected.click(function(e) {
                var isHidden = selector.hasClass('hide');
                $('.item-selector').addClass('hide');
                if (isHidden) {
                    selector.removeClass('hide');
                }
                else {
                    selector.addClass('hide');
                }
                e.preventDefault();
            });

            items.click(function(e) {
                var item = $(this);
                var text = item.html();
                selected.html(text);
                selected.trigger('click');
                e.preventDefault();
            });
        });

    };

})(jQuery);

$(document).ready(function() {

    $('.table.navigatable').navigatable();
    $('.toggle-button').moreLess();
    $('.calendar-select').dropDown();
    
    $('#lo').tooltip();
});

